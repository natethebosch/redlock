# Redlock

Redlock is a Golang package that implements the redlock distributed lock algorithm as described in the [redis docs](https://redis.io/topics/distlock). 
This package also implements the [redis cluster hash slot calculation](https://redis.io/topics/cluster-spec).

### Installation
```
go get bitbucket.org/natethebosch/redlock
```

### Usage
```golang
func ExampleRedLock() {
	var db *redis.ClusterClient

	// blocks until lock is acquired or context times out
	lc, err := Lock(context.Background(), db, "some-key", 5*time.Second)
	if err != nil {
		// context timed out
		return
	}

	// do protected things

	// dispose of the lock
	lc.Unlock()
}
```

### Note
Adapted from github.com/natethebosch/redlock 