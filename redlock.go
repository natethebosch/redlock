package redlock

import (
	"context"
	"errors"
	"math/rand"
	"strconv"
	"time"

	redis "gopkg.in/redis.v5"
)

const retryDelay = time.Millisecond

type DistributedLock interface {
	Unlock()
}

type lock struct {
	random int
	keys   []string
	db     *redis.ClusterClient
}

// Captures a distributed lock at the key provided using a context.
// Duration specifies how long the lock will be held, the actual lock duration will be less than the value provided
// because of the delay in locking each node in the cluster.
// Errors will only originate from the context timeout.
func Lock(ctx context.Context, db *redis.ClusterClient, key string, duration time.Duration) (DistributedLock, error) {

	lc := &lock{
		db: db,
	}

	err := lc.Lock(ctx, db, key, duration)
	if err != nil {
		return nil, err
	}

	return lc, nil
}

func (r *lock) Lock(ctx context.Context, db *redis.ClusterClient, key string, duration time.Duration) error {

	for {
		err := r.lock(db, key, duration)

		if err != nil {

			select {
			case <-ctx.Done():
				return ctx.Err()
			case <-time.After(retryDelay):
				continue
			}

		}

		return nil
	}
}

func (r *lock) lock(db *redis.ClusterClient, key string, duration time.Duration) error {

	r.random = rand.Int()

	slots, err := db.ClusterSlots().Result()
	if err != nil {
		return err
	}

	r.keys = r.keys[0:0]

	successCount := 0

	for _, slot := range slots {

		slotKey, err := keyWithinSlotRange(key, slot.Start, slot.End)
		if err != nil {
			r.Unlock()
			return err
		}

		ok, err := db.SetNX(slotKey, r.random, duration).Result()
		if ok && err == nil {
			successCount++
			r.keys = append(r.keys, slotKey)
		}
	}

	if successCount <= (len(slots) / 2) {
		r.Unlock()
		return errors.New("failed to capture lock")
	}

	return nil
}

// Releases the lock
func (r *lock) Unlock() {

	for _, key := range r.keys {

		// ignore errors
		r.db.Eval(`if redis.call("get",KEYS[1]) == ARGV[1] then
			    return redis.call("del",KEYS[1])
			else
			    return 0
			end`, []string{key}, r.random)

	}

	r.keys = r.keys[0:0]
}

var unableToComputeKey = errors.New("unable to compute key within slot range in a reasonable number of iterations")

func keyWithinSlotRange(baseKey string, min, max int) (string, error) {

	var key string
	var slot int

	for i := 0; i < 10000; i++ {
		key = "{" + strconv.Itoa(i) + "}" + baseKey
		slot = computeHashSlot(key)

		if slot >= min && slot <= max {
			return key, nil
		}
	}

	return "", unableToComputeKey
}
