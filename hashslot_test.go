package redlock

import (
	"testing"

	redis "gopkg.in/redis.v5"
)

func TestComputeHashSlot_TrivialCase(t *testing.T) {

	sampleKey := "mykey"

	calc := computeHashSlot(sampleKey)

	cli := redis.NewClusterClient(&redis.ClusterOptions{
		Addrs: testRedisInstances,
	})

	expected, err := cli.ClusterKeySlot(sampleKey).Result()
	if err != nil {
		t.Fatal(err)
	}

	if int64(calc) != expected {
		t.Fatal("expected", expected, "got", calc)
	}
}

func TestComputeHashSlot_ValidBraces(t *testing.T) {

	sampleKey := "my{key}"

	calc := computeHashSlot(sampleKey)

	cli := redis.NewClusterClient(&redis.ClusterOptions{
		Addrs: testRedisInstances,
	})

	expected, err := cli.ClusterKeySlot(sampleKey).Result()
	if err != nil {
		t.Fatal(err)
	}

	if int64(calc) != expected {
		t.Fatal("expected", expected, "got", calc)
	}
}

func TestComputeHashSlot_NotSpecialBraces(t *testing.T) {

	sampleKey := "my{}"

	calc := computeHashSlot(sampleKey)

	cli := redis.NewClusterClient(&redis.ClusterOptions{
		Addrs: testRedisInstances,
	})

	expected, err := cli.ClusterKeySlot(sampleKey).Result()
	if err != nil {
		t.Fatal(err)
	}

	if int64(calc) != expected {
		t.Fatal("expected", expected, "got", calc)
	}
}

func TestComputeHashSlot_InvalidBraces(t *testing.T) {

	sampleKey := "my}key{"

	calc := computeHashSlot(sampleKey)

	cli := redis.NewClusterClient(&redis.ClusterOptions{
		Addrs: testRedisInstances,
	})

	expected, err := cli.ClusterKeySlot(sampleKey).Result()
	if err != nil {
		t.Fatal(err)
	}

	if int64(calc) != expected {
		t.Fatal("expected", expected, "got", calc)
	}
}

func BenchmarkcomputeHashSlot(b *testing.B) {

	sampleKey := "my}key{"

	for i := 0; i < b.N; i++ {
		_ = computeHashSlot(sampleKey)
	}
}
