package redlock

import (
	"context"
	"testing"
	"time"

	redis "gopkg.in/redis.v5"
)

var testRedisInstances = []string{"192.168.1.2:7000", "192.168.1.2:7001", "192.168.1.2:7002"}

func ExampleRedLock() {
	var db *redis.ClusterClient

	// blocks until lock is acquired or context times out
	lc, err := Lock(context.Background(), db, "some-key", 5*time.Second)
	if err != nil {
		// context timed out
		return
	}

	// do protected things

	// dispose of the lock
	lc.Unlock()
}

func TestRedLock_Lock(t *testing.T) {

	db := redis.NewClusterClient(&redis.ClusterOptions{
		Addrs: testRedisInstances,
	})

	done := make(chan bool)

	go func() {

		lc, _ := Lock(context.Background(), db, "mykey", time.Second)
		time.Sleep(500 * time.Millisecond)
		lc.Unlock()

		done <- true
	}()

	go func() {
		lc, _ := Lock(context.Background(), db, "mykey", time.Second)
		time.Sleep(500 * time.Millisecond)
		lc.Unlock()

		done <- true
	}()

	start := time.Now()

	<-done
	<-done

	duration := time.Now().Sub(start)

	if duration < 1*time.Second {
		t.Fatal("locks aren't working, should take at least 1 second")
	}

	if duration >= 2*time.Second {
		t.Fatal("locks aren't unlocking, shouldn't take 2 seconds")
	}

}

func TestRedLock_LockUnLock(t *testing.T) {

	db := redis.NewClusterClient(&redis.ClusterOptions{
		Addrs: testRedisInstances,
	})

	start := time.Now()

	lc, _ := Lock(context.Background(), db, "mykey", time.Second)
	lc.Unlock()

	lc, _ = Lock(context.Background(), db, "mykey", time.Second)
	lc.Unlock()

	if int(time.Now().Sub(start).Seconds()) > 0 {
		t.Fatal("should be less than a second else the lock is not being unlocked", time.Now().Sub(start))
	}
}
